use std::env;
use std::fs;
use std::io::{stdout, Write};

use crossterm::{
	cursor::{position, MoveUp, MoveLeft, MoveRight, MoveDown, MoveTo, MoveToColumn, MoveToNextLine},
	event::{read, Event, KeyEvent, KeyCode},
	execute,
	queue,
	style::{Attribute, Color, Print, ResetColor, SetAttribute, SetForegroundColor},
	terminal::{Clear, ClearType, EnterAlternateScreen, LeaveAlternateScreen, ScrollUp}
};


fn main() {
	// Read terminal parameters
	let args: Vec<String> = env::args().collect();
	let mut stdout = stdout();

	if args.len() != 2 {
		queue!(
			stdout,
			SetAttribute(Attribute::Bold),
			SetForegroundColor(Color::DarkRed),
			Print("Error:"),
			SetAttribute(Attribute::Reset),
			ResetColor,
			Print(" Expected 1 argument (a path to a file)\n")
		).expect("Failed to error out");

		return
	}


	// Read from file
	let file_content = fs::read_to_string(&args[1]).expect("Failed to read file");
	// Convert line endings to unix like
	let converted = file_content.replace("\r", "");
	let file_lines: Vec<&str> = converted.lines().collect();
	let mut lines: Vec<String> = vec![String::from(""); file_lines.len()];
	let mut i: usize = 0;

	crossterm::terminal::enable_raw_mode().expect("Failed to use enter raw mode");
	execute!(stdout, EnterAlternateScreen, Clear(ClearType::All), MoveTo(0, 0)).expect("Failed to enter alternate screen");

	while i < file_lines.len() {
		lines[i] = file_lines[i].to_string();
		queue!(stdout, Print(&lines[i]), MoveToNextLine(1)).expect("Failed to use Enter");
		i += 1;
	}

	queue!(stdout, MoveTo(0, 0)).expect("Failed to move");

	loop {
		let cursor_pos = position().expect("Could not get cursor position");

		match read().expect("Failed to read") {
			Event::Key(KeyEvent {code, modifiers}) => {
				match code {
					KeyCode::Char(c) => {
						lines[cursor_pos.1 as usize].insert(cursor_pos.0 as usize, c);
						queue!(stdout, Print(&lines[cursor_pos.1 as usize][(cursor_pos.0 as usize)..]), MoveToColumn(cursor_pos.0 + 2)).expect("Failed to use CHAR");
						stdout.flush().expect("Failed to flush");
					}
					KeyCode::Up => {
						if cursor_pos.1 > 0 {
							if (cursor_pos.0 as usize) > lines[cursor_pos.1 as usize - 1].len() {
								queue!(stdout, MoveTo((lines[cursor_pos.1 as usize - 1].len()) as u16, cursor_pos.1 - 1)).expect("Failed to use UP");
							}
							else {
								queue!(stdout, MoveUp(1)).expect("Failed to use UP");
							}
						}
						else {
							queue!(stdout, MoveToColumn(0)).expect("Failed to use UP");
						}

						stdout.flush().expect("Failed to flush");
					}
					KeyCode::Left => {
						if cursor_pos.0 < 1 && cursor_pos.1 > 0 {
							queue!(stdout, MoveTo((lines[cursor_pos.1 as usize - 1].len()) as u16, cursor_pos.1 - 1)).expect("Failed to use LEFT");
						}
						else {
							queue!(stdout, MoveLeft(1)).expect("Failed to use LEFT");
						}

						stdout.flush().expect("Failed to flush");
					}
					KeyCode::Right => {
						if lines.len() > 0 {
							if cursor_pos.0 < lines[cursor_pos.1 as usize].len() as u16 {
								queue!(stdout, MoveRight(1)).expect("Failed to use RIGHT");
							}
							else {
								if cursor_pos.1 < lines.len() as u16 - 1 {
									queue!(stdout, MoveToNextLine(1)).expect("Failed to use RIGHT");
								}
							}
						}

						stdout.flush().expect("Failed to flush");
					}
					KeyCode::Down => {
						if lines.len() > 0 {
							if (cursor_pos.1 as usize) < lines.len() as usize - 1 {
								if (cursor_pos.0 as usize) > lines[cursor_pos.1 as usize + 1].len() {
									queue!(stdout, MoveTo(lines[cursor_pos.1 as usize + 1].len() as u16, cursor_pos.1 + 1)).expect("Failed to use DOWN");
								}
								else {
									queue!(stdout, MoveDown(1)).expect("Failed to use DOWN");
								}
							}
							else {
								queue!(stdout, MoveToColumn(lines[cursor_pos.1 as usize].len() as u16 + 1)).expect("Failed to use DOWN");
							}
						}

						stdout.flush().expect("Failed to flush");
					}
					KeyCode::Enter => {
						if (cursor_pos.0 as usize) < lines.len() {
							// If enter is pressed in between text
							// Text needs to be split in 2 strings and add an empty string between;
							let (str1, str2) = lines[cursor_pos.1 as usize].split_at(cursor_pos.0 as usize);
							let str11 = String::from(str1);
							let str22 = String::from(str2);
							lines.remove(cursor_pos.1 as usize);
							lines.insert(cursor_pos.1 as usize, str11);
							lines.insert(cursor_pos.1 as usize + 1, str22);
							queue!(stdout, MoveToNextLine(1)).expect("Failed to use ENTER");
						}
						else {
							// If enter is pressed at the end of string
							// Insert an empty string after it
							lines.insert(cursor_pos.1 as usize + 1, "".to_string());
							queue!(stdout, MoveToNextLine(1)).expect("Failed to use ENTER");
						}

						stdout.flush().expect("Failed to flush");
					}
					KeyCode::Tab => {
						// tab size from config
						lines[cursor_pos.1 as usize].insert(0, '\t');
						queue!(stdout, Print("        "), Print(&lines[cursor_pos.1 as usize]), MoveToColumn(cursor_pos.0 + 1)).expect("Failed to use TAB");
						stdout.flush().expect("Failed to flush");
					}
					KeyCode::BackTab => {
						// tab size from config
						// if line has tabs
						// lines[cursor_pos.1 as usize].remove(0);

						stdout.flush().expect("Failed to flush");
					}
					KeyCode::Backspace => {
						if lines.len() > 0 {
							if cursor_pos.0 > 0 {
								lines[cursor_pos.1 as usize].remove(cursor_pos.0 as usize - 1);
								queue!(stdout, MoveLeft(1), Print(" "), MoveLeft(1)).expect("Failed to use BACKSPACE");
							}
							else {
								if cursor_pos.1 > 0 {
									queue!(stdout, MoveTo((lines[cursor_pos.1 as usize - 1].len()) as u16, cursor_pos.1 - 1)).expect("Failed to use BACKSPACE");
									lines[cursor_pos.1 as usize - 1] = format!("{}{}", lines[cursor_pos.1 as usize - 1], lines[cursor_pos.1 as usize]);
									lines.remove(cursor_pos.1 as usize);
									let col = cursor_pos.0;
									let row = cursor_pos.1;
									let mut index = row as usize - 1;

									while index < lines.len() {
										queue!(
											stdout,
											MoveToColumn(0),
											Clear(ClearType::CurrentLine),
											Print(&lines[index]),
											MoveToNextLine(1)
										).expect("Failed to use Clear");
										index += 1;
									}

									queue!(stdout, Clear(ClearType::CurrentLine), MoveTo(col, row - 1)).expect("Failed to use BACKSPACE");
								}
							}
						}

						stdout.flush().expect("Failed to flush");
					}
					KeyCode::Delete => {
						if lines.len() > 0 {
							if cursor_pos.0 < lines[cursor_pos.1 as usize].len() as u16 {
								lines[cursor_pos.1 as usize].remove(cursor_pos.0 as usize);
								queue!(
									stdout,
									Clear(ClearType::CurrentLine),
									Print(&lines[cursor_pos.1 as usize]),
									MoveToColumn(cursor_pos.0 + 1)
								).expect("Failed to use DELETE");
							}
							else {
								if cursor_pos.1 < lines.len() as u16 - 1 {
									let col = cursor_pos.0;
									let row = cursor_pos.1;
									let mut index = row as usize;

									lines[cursor_pos.1 as usize] = format!("{}{}", lines[cursor_pos.1 as usize], lines[cursor_pos.1 as usize + 1]);
									lines.remove(cursor_pos.1 as usize + 1);

									while index < lines.len() {
										queue!(stdout, Print(&lines[index]), MoveToNextLine(1), Clear(ClearType::CurrentLine)).expect("Failed to use Enter");
										index += 1;
									}

									queue!(stdout, MoveTo(col, row)).expect("Failed to use DELETE");
								}
							}
						}

						stdout.flush().expect("Failed to flush");
					}
					KeyCode::Home => {
						queue!(stdout, MoveToColumn(0)).expect("Failed to use HOME");
						stdout.flush().expect("Failed to flush");
					}
					KeyCode::End => {
						queue!(stdout, MoveToColumn(lines[cursor_pos.1 as usize].len() as u16 + 1)).expect("Failed to use END");
						stdout.flush().expect("Failed to flush");
					}
					KeyCode::PageUp => {
						break
					}
					KeyCode::PageDown => {
						break
					}
					KeyCode::Esc => {
						// Promt to save
						// Exit - close file
						break
					}
					_ => {
						continue
					}
				}
			}
			Event::Mouse(_) => {
				continue
			}
			Event::Resize(_, _) => {
				continue
			}
		}
	}

	crossterm::terminal::disable_raw_mode().expect("Failed to disable raw mode");
	execute!(stdout, LeaveAlternateScreen).expect("Failed to leave alternate screen");

	// Trim and write changes to file (at the initial location)
	i = 0;

	while i < lines.len() {
		lines[i] = lines[i].trim_end().to_string();
		i += 1;

		if lines.len() > 1 && lines[lines.len() - 1].len() < 1 {
			lines.pop();
		}
	}

	let mut modified_lines = lines.join("\n");
	modified_lines.push('\n');

	if modified_lines.len() != file_content.len() {
		// fs::write(&args[1], modified_lines).expect("Failed to write file");
		fs::write("text2.txt", modified_lines).expect("Failed to write file");
	}

	return
}
